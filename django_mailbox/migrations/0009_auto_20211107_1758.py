# Generated by Django 2.2.4 on 2021-11-07 14:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('django_mailbox', '0008_auto_20190219_1553'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailbox',
            name='uri',
            field=models.CharField(blank=True, default=None, help_text="Example: imap+ssl://myusername:mypassword@someserver <br /><br />Internet transports include 'imap' and 'pop3'; common local file transports include 'maildir', 'mbox', and less commonly 'babyl', 'mh', and 'mmdf'. <br /><br />Be sure to urlencode your username and password should they contain illegal characters (like @, :, etc).", max_length=500, null=True, verbose_name='URI'),
        ),
    ]
