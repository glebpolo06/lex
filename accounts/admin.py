from django.contrib import admin
from .models import Customer
from mails.models import SMTPAccount


class SMTPAccountInline(admin.StackedInline):
    extra = 0
    model = SMTPAccount


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'user',
        'role',
    )
    inlines = [
        SMTPAccountInline,
    ]
