# Generated by Django 2.2.4 on 2021-10-23 19:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_customer_dh_a'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='dh_A',
            field=models.CharField(max_length=255, null=True, verbose_name='DH, открытый ключ А'),
        ),
    ]
