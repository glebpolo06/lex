from django.views.generic import (
    FormView,
)
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from PIL import Image
from .forms import (
    ExtendUserCreationForm,
    PersonalForm
)


class SignUpView(FormView):
    template_name = "registration/signup.html"
    form_class = ExtendUserCreationForm
    success_url = reverse_lazy('accounts:login')
    redirect_authenticated_user = True

    def dispatch(self, request, *args, **kwargs):
        f = ExtendUserCreationForm()
        if self.redirect_authenticated_user and self.request.user.is_authenticated:
            redirect_to = self.get_success_url()
            if redirect_to == self.request.path:
                raise ValueError(
                    "Redirection loop for authenticated user detected. Check that "
                    "your LOGIN_REDIRECT_URL doesn't point to a login page."
                )
            return HttpResponseRedirect(redirect_to)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.save()
        user.customer.name = form.cleaned_data['name']
        user.customer.dh_A = form.cleaned_data['dh_A']
        user.customer.save()
        user.save()
        # messages.success(self.request, "Регистрация прошла успешно")
        return super().form_valid(form)


class PersonalView(FormView):
    template_name = 'personal/pages/personal_data.html'
    form_class = PersonalForm
    success_url = reverse_lazy('accounts:personal')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = self.request.user.customer
        return kwargs

    def form_valid(self, form):
        customer = form.save()
        if customer.avatar != '':
            avatar = Image.open(customer.avatar.path)
            avatar = avatar.resize((300, 300))
            avatar.save(customer.avatar.path)
            customer.save()
        return super().form_valid(form)
