from django import forms
from django.contrib.auth.models import User
from django.utils.translation import gettext, gettext_lazy as _
from django.contrib.auth.forms import UserCreationForm
from .models import Customer


class ExtendUserCreationForm(UserCreationForm):
    name = forms.CharField(
        label=_("Имя"),
        max_length=100,
        required=False,
        widget=forms.TextInput(attrs={'class': 'input100'}),
    )
    dh_A = forms.CharField(
        widget=forms.HiddenInput(),
        required=True
    )

    class Meta:
        model = User
        fields = ('username', 'name', 'password1', 'password2', 'dh_A')
        widgets = {
            'username': forms.TextInput(attrs={'class': 'input100'}),
            'name': forms.TextInput(attrs={'class': 'input100'}),
            'password1': forms.PasswordInput(attrs={'class': 'input100'}),
        }


class PersonalForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ('name', 'facebook', 'instagram', 'twitter', 'avatar')
        widgets = {
           'name': forms.TextInput(attrs={'class': 'input100 font-fix-regular'}),
           'facebook': forms.TextInput(attrs={'class': 'input100 font-fix-regular'}),
           'twitter': forms.TextInput(attrs={'class': 'input100 font-fix-regular'}),
           'instagram': forms.TextInput(attrs={'class': 'input100 font-fix-regular'}),
        }
