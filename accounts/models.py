from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from admintools.models import CoreModel


class Customer(CoreModel):
    user = models.OneToOneField(
        User,
        null=True,
        on_delete=models.CASCADE,
    )
    name = models.CharField(
        'Имя',
        max_length=255,
        null=True,
    )
    ROLE_CHOICES = [
        ('creator', 'Администратор'),
        ('moderator', 'Модератор'),
        ('user', 'Пользователь'),
    ]
    role = models.CharField(
        max_length=40,
        choices=ROLE_CHOICES,
        verbose_name='Роль',
        default='user',
    )
    avatar = models.ImageField(
        'Аватар',
        upload_to='avatars',
        blank=True,
        null=True,
    )
    facebook = models.URLField(
        'Ссылка на аккаунт в Facebook',
        max_length=100,
        null=True,
        blank=True,
    )
    twitter = models.URLField(
        'Ссылка на аккаунт в Twitter',
        max_length=100,
        null=True,
        blank=True,
    )
    instagram = models.URLField(
        'Ссылка на аккаунт в Instagram',
        max_length=100,
        null=True,
        blank=True,
    )

    dh_A = models.CharField(
        'DH, открытый ключ А',
        max_length=1000,
        null=True,
    )

    def __str__(self):
        return self.user.username

    class Meta:
        ordering = ['created']
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, **kwargs):
    if kwargs['created']:
        Customer.objects.create(user=instance)
