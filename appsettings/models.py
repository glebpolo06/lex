from django.db import models
from preferences.models import Preferences
from admintools.models import CoreModel
from .pyDH import DiffieHellman


class AppSettings(Preferences):
    dh_a = models.DecimalField(
        'Закрытый ключ сервера, а',
        help_text='Закрытый ключ для ДХ',
        max_digits=100,
        decimal_places=0,
        null=True,
        blank=True,
    )

    def save(self, *args, **kwargs):
        if not self.dh_a:
            self.dh_a = self.generate_dh_a()
        return super(AppSettings, self).save(*args, **kwargs)

    def generate_dh_a(self):
        return DiffieHellman().get_private_key()

    @property
    def dh(self):
        return DiffieHellman(a=int(self.dh_a))

    class Meta:
        verbose_name = 'Настройки'
        verbose_name_plural = 'Настройки'
