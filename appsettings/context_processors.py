from .models import AppSettings


def get_appsettings(request):
    return {'appsettings': AppSettings.objects.first()}
