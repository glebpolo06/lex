from django.contrib import admin
from preferences.admin import PreferencesAdmin
from appsettings.models import (
    AppSettings,
)


@admin.register(AppSettings)
class AppSettingsAdmin(PreferencesAdmin):
    pass
