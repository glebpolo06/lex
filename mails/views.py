from django.shortcuts import render
from django.views.generic import (
    FormView,
    DetailView,
)
from django.http import JsonResponse, HttpResponseRedirect
from django.views.generic.list import MultipleObjectMixin
from django.urls import reverse_lazy, reverse
from django.db.models import Q
from django.conf import settings
from django_mailbox.models import Message
from accounts.models import Customer
from .models import SMTPAccount
from .forms import SMTPAccountForm


class SMTPAccountCreateView(FormView):
    template_name = 'personal/pages/smtp_account.html'
    form_class = SMTPAccountForm
    success_url = reverse_lazy('accounts:personal')

    '''def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = self.request.user.customer
        return kwargs'''

    def form_valid(self, form):
        smtp_account = form.save(commit=False)
        smtp_account.customer = self.request.user.customer
        smtp_account.set_password(smtp_account.password, save=False)
        smtp_account.save()
        return super().form_valid(form)


class SMTPAccountEditView(DetailView):
    model = SMTPAccount
    template_name = 'personal/pages/smtp_account_edit.html'

    def post(self, *args, **kwargs):
        smtp_acc = self.get_object()
        new_password = self.request.POST.get('new_password')
        smtp_acc.set_password(new_password)
        return HttpResponseRedirect(reverse("accounts:personal"))

    '''def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = self.get_object()
        return kwargs

    def form_valid(self, form):
        smtp_account = form.save(commit=False)
        smtp_account.save()
        return super().form_valid(form)'''


# почта, отправка письма
class SMTPAccountDetailView(DetailView, MultipleObjectMixin):
    model = SMTPAccount
    template_name = 'mails/pages/mail.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        self.get_object().fetch_messages()
        self.get_object_list()
        context = super(SMTPAccountDetailView, self).get_context_data(**kwargs)
        context['account'] = self.get_object()
        return context

    def get_object_list(self):
        smtp_acc = self.get_object()
        self.object_list = smtp_acc.mailbox.messages.all().order_by(
            '-processed',
        )
        folder = self.request.GET.get('folder')
        search = self.request.GET.get('search')
        # отправленные
        if folder == 'sent':
            self.object_list = self.object_list.filter(
                from_header__icontains=smtp_acc.default_from
            )
        # поиск (ищет по всему)
        elif search:
            self.object_list = self.object_list.filter(
                Q(subject__icontains=search) |
                Q(body__icontains=search),
            )
        # входящие
        else:
            self.object_list = self.object_list.exclude(
                from_header__icontains=smtp_acc.default_from
            )

    def post(self, *args, **kwargs):
        smtp_acc = self.get_object()
        subject = self.request.POST.get('subject', '')
        content = self.request.POST.get('message', '')
        to = self.request.POST.get('to', '')
        files = self.request.FILES.getlist('attachments')
        # print(self.request.FILES)
        smtp_acc.send(
            subject,
            content,
            [to],
            files=files,
        )
        return HttpResponseRedirect(
            reverse(
                "mails:detail_smtp_acc",
                kwargs={'pk': self.get_object().pk}
            )
        )


# почта, отправка письма
class MessageDetailView(DetailView):
    model = Message
    template_name = 'mails/pages/read.html'

    def get_context_data(self, **kwargs):
        context = super(MessageDetailView, self).get_context_data(**kwargs)
        context['account'] = self.get_object().mailbox.smtpaccount_set.filter(
            customer=self.request.user.customer,
        ).first()
        return context


def email_autocomplete(request, *args, **kwargs):
    data = {}
    suggestions = []
    q = request.GET.get('query', '')
    smtp_accounts = SMTPAccount.objects.filter(
        default_from__icontains=q
    )
    for smtp_account in smtp_accounts:
        suggestions.append({
            'value': smtp_account.default_from,
            'data': {
                'dh_A': smtp_account.customer.dh_A,
            }
        })
    data = {'suggestions': suggestions}
    return JsonResponse(data)
