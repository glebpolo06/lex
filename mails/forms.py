from django import forms
from django.utils.translation import gettext, gettext_lazy as _
from .models import SMTPAccount, SMTP


class SMTPAccountForm(forms.ModelForm):
    smtp = forms.ModelChoiceField(
                queryset=SMTP.objects.all(),
                label=_("Почтовый сервис"),
                widget=forms.Select(attrs={'class': 'input100 font-fix-regular'}),
            )

    class Meta:
        model = SMTPAccount
        fields = (
            'smtp', 'default_from', 'password',
        )
        labels = {
            'default_from': _('E-mail'),
            # 'username': _('Хост'),
            'password': _('Пароль')
        }
        widgets = {
            'default_from': forms.TextInput(attrs={'class': 'input100 font-fix-regular'}),
            # 'username': forms.TextInput(attrs={'class': 'input100 font-fix-regular'}),
            'password': forms.TextInput(attrs={'class': 'input100 font-fix-regular', 'autocomplete': 'off'}),
        }
