from django.db import models
from django.core.mail import EmailMessage
from django.core.mail.backends.smtp import EmailBackend
from django.core.validators import ValidationError
from django.urls import reverse
from django.conf import settings
from django_mailbox.models import Mailbox
import imaplib
import time
import urllib.parse
from admintools.models import CoreModel


class SMTP(CoreModel):
    name = models.CharField(
        'Name',
        max_length=255,
    )
    host = models.CharField(
        'Host',
        help_text='SMTP host',
        max_length=255,
    )
    port = models.IntegerField(
        'Port',
    )
    use_tls = models.BooleanField(
        'Use TLS',
        default=True,
    )
    use_ssl = models.BooleanField(
        'Use SSL',
        default=False,
    )
    timeout = models.IntegerField(
        'Timeout',
        null=True,
        blank=True,
    )

    host_receiving = models.CharField(
        'Host Receiving',
        help_text='IMAP, POP3, etc.',
        max_length=255,
    )
    host_imap = models.CharField(
        'Host IMAP',
        max_length=255,
    )
    sent_folder = models.CharField(
        '"Sent folder" name',
        help_text='It is key which IMAP uses to identify this folder',
        default='Sent',
        max_length=255,
    )

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        return super(SMTP, self).save(*args, **kwargs)

    def clean(self, *args, **kwargs):
        if self.use_tls == self.use_ssl:
            raise ValidationError('Должно быть включено либо SSL, либо TLS')
        super(SMTP, self).clean(*args, **kwargs)

    class Meta:
        verbose_name = 'SMTP'
        verbose_name_plural = 'SMTP'


class SMTPAccount(CoreModel):
    smtp = models.ForeignKey(
        SMTP,
        verbose_name='SMTP',
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        'accounts.Customer',
        verbose_name='Клиент',
        on_delete=models.CASCADE,
    )
    mailbox = models.ForeignKey(
        'django_mailbox.Mailbox',
        verbose_name='Mailbox',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    default_from = models.CharField(
        'From',
        help_text='Отправитель',
        max_length=255,
        unique=True,
    )
    username = models.CharField(
        'Username',
        max_length=255,
    )
    password = models.CharField(
        'Password',
        help_text='Encrypted',
        max_length=255,
    )

    def __str__(self):
        return self.default_from

    def save(self, *args, **kwargs):
        if not self.username:
            self.username = self.default_from
        if not self.mailbox:
            self.connect_mailbox(save=False)
        cur_obj = SMTPAccount.objects.filter(id=self.id).first()
        if not cur_obj:
            return super(SMTPAccount, self).save(*args, **kwargs)
        if cur_obj.password != self.password or cur_obj.username != self.username:
            self.connect_mailbox(save=False)
        return super(SMTPAccount, self).save(*args, **kwargs)

    def get_password(self):
        return settings.CIPHER.decrypt(self.password)

    def set_password(self, password, save=True):
        if not password:
            return
        self.password = settings.CIPHER.encrypt(
            password
        ).decode('utf-8')
        if save:
            self.save()

    def send(self, subject, content, emails, files=[]):
        smtp_backend = self.get_backend()
        email = EmailMessage(
            subject,
            content,
            self.default_from,
            emails,
            headers={'Lex-Client': 'Yes'},
            connection=smtp_backend,
        )
        for file in files:
            email.attach(file.name, file.read(), file.content_type)
        email.send()
        message = email.message()
        imap_backend = self.get_backend(type='imap')
        # print(message.as_string())
        imap_backend.append(
            self.smtp.sent_folder,
            '\\Seen',
            imaplib.Time2Internaldate(time.time()),
            message.as_string().encode('utf8'),
        )
        imap_backend.logout()

    def get_backend(self, type='smtp'):
        if type == 'smtp':
            return EmailBackend(
                host=self.smtp.host,
                port=25,
                username=self.username,
                password=self.get_password(),
                use_tls=self.smtp.use_tls,
                use_ssl=self.smtp.use_ssl,
                timeout=self.smtp.timeout,
                # fail_silently=self.fail_silently
            )
        username = self.username
        password = self.get_password()
        mail_server = self.smtp.host_imap
        imap_server = imaplib.IMAP4_SSL(host=mail_server)
        imap_server.login(username, password)
        return imap_server

    def get_edit_url(self):
        return reverse(
                "mails:edit_smtp_acc",
                kwargs={"pk": self.id}
            )

    def get_detail_url(self):
        return reverse(
                "mails:detail_smtp_acc",
                kwargs={"pk": self.id}
            )

    def connect_mailbox(self, save=True):
        protocol = self.smtp.host_receiving.split('.')[0]
        if 'pop' in protocol and '3' not in protocol:
            protocol += '3'
        prefix = ''
        if 'gmail' in self.smtp.host_receiving:
            prefix = 'recent%3A'
        uri = '{protocol}+ssl://{prefix}{username}:{password}@{server}'.format(
            protocol=protocol,
            prefix=prefix,
            username=urllib.parse.quote_plus(self.default_from),
            password=self.password,
            server=self.smtp.host_receiving,
        )
        if not self.mailbox:
            self.mailbox = Mailbox.objects.create(
                uri=uri,
                name='{}: {}'.format(self.customer.id, self.default_from),
            )
        else:
            self.mailbox.uri = uri
            self.mailbox.save()
        if save:
            self.save()
        return self.mailbox

    # грузит еще незагруженные сообщения
    def fetch_messages(self):
        return sum(1 for i in self.mailbox.get_new_mail())

    class Meta:
        verbose_name = 'SMTP аккаунт'
        verbose_name_plural = 'SMTP аккаунты'
