from django.contrib import admin
from .models import (
    SMTP,
    SMTPAccount,
)


@admin.register(SMTP)
class SMTPAdmin(admin.ModelAdmin):
    pass
