# Generated by Django 2.2.4 on 2021-09-19 18:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('django_mailbox', '0008_auto_20190219_1553'),
        ('mails', '0003_smtp_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='smtpaccount',
            name='mailbox',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='django_mailbox.Mailbox', verbose_name='Mailbox'),
        ),
        migrations.AlterField(
            model_name='smtpaccount',
            name='default_from',
            field=models.CharField(help_text='Отправитель', max_length=255, verbose_name='From'),
        ),
    ]
