# Generated by Django 2.2.4 on 2021-09-19 16:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mails', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='smtp',
            name='host_receiving',
            field=models.CharField(default='pop3.yandex.com', help_text='IMAP, POP3, etc.', max_length=255, verbose_name='Host Receiving'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='smtp',
            name='host',
            field=models.CharField(help_text='SMTP host', max_length=255, verbose_name='Host'),
        ),
    ]
