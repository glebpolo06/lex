# Generated by Django 2.2.4 on 2021-10-27 21:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mails', '0005_smtp_host_imap'),
    ]

    operations = [
        migrations.AlterField(
            model_name='smtpaccount',
            name='default_from',
            field=models.CharField(help_text='Отправитель', max_length=255, unique=True, verbose_name='From'),
        ),
    ]
