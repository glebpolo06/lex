from django.urls import path
from .views import (
    SMTPAccountCreateView,
    SMTPAccountEditView,
    SMTPAccountDetailView,
    MessageDetailView,
    email_autocomplete,
)

app_name = 'mails'

urlpatterns = [
    path('create/', SMTPAccountCreateView.as_view(), name='create_smtp_acc'),
    path('edit/<int:pk>/', SMTPAccountEditView.as_view(), name='edit_smtp_acc'),
    path('messages/<int:pk>/', MessageDetailView.as_view(), name='detail_message'),
    path('email_autocomplete', email_autocomplete, name='email_autocomplete'),
    path('<int:pk>/', SMTPAccountDetailView.as_view(), name='detail_smtp_acc'),
]
