function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function createFileList(a) {
  a = [].slice.call(Array.isArray(a) ? a : arguments)
  for (var c, b = c = a.length, d = !0; b-- && d;) d = a[b] instanceof File
  if (!d) throw new TypeError('expected argument to FileList is File or array of File objects')
  for (b = (new ClipboardEvent('')).clipboardData || new DataTransfer; c--;) b.items.add(a[c])
  return b.files
}

function arrayBufferToBase64( buffer ) {
    var binary = '';
    var bytes = new Uint8Array( buffer );
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode( bytes[ i ] );
    }
    return window.btoa( binary );
}

function base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

function bytesArrToBase64(arr) {
  const abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; // base64 alphabet
  const bin = n => n.toString(2).padStart(8,0); // convert num to 8-bit binary string
  const l = arr.length
  let result = '';

  for(let i=0; i<=(l-1)/3; i++) {
    let c1 = i*3+1>=l; // case when "=" is on end
    let c2 = i*3+2>=l; // case when "=" is on end
    let chunk = bin(arr[3*i]) + bin(c1? 0:arr[3*i+1]) + bin(c2? 0:arr[3*i+2]);
    let r = chunk.match(/.{1,6}/g).map((x,j)=> j==3&&c2 ? '=' :(j==2&&c1 ? '=':abc[+('0b'+x)]));
    result += r.join('');
  }

  return result;
}

function hashPassword(password) {
  let arrayHash = sjcl.misc.pbkdf2(password, password, 15000);
  let b64Hash = bytesArrToBase64(arrayHash);
  return b64Hash;
}

const a_from_pass = function(str, seed = 0) {
    let h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed;
    for (let i = 0, ch; i < str.length; i++) {
        ch = str.charCodeAt(i);
        h1 = Math.imul(h1 ^ ch, 2654435761);
        h2 = Math.imul(h2 ^ ch, 1597334677);
    }
    h1 = Math.imul(h1 ^ (h1>>>16), 2246822507) ^ Math.imul(h2 ^ (h2>>>13), 3266489909);
    h2 = Math.imul(h2 ^ (h2>>>16), 2246822507) ^ Math.imul(h1 ^ (h1>>>13), 3266489909);
    return bigInt(bigInt(429496729618956235478646719384038284939224906446895) * (bigInt(20971513217543545453) & h2) + (h1>>>0));
};

function getMessageEncoding(selector, string=false) {
    if (string) {
      var message = selector;
    } else {
      var messageBox = document.querySelector(selector);
      var message = messageBox.value;
    };
    var enc = new TextEncoder();
    return enc.encode(message);
  }

  function getMessageDecoding(array) {
      const dec = new TextDecoder();
      return dec.decode(array);
    }

function encryptMessage(encoded, secretKey) {
  const iv = new Uint8Array(16);
  //const iv = window.crypto.getRandomValues(new Uint8Array(12))
  const ciphertext = window.crypto.subtle.encrypt(
    {
      name: "AES-CBC",
      iv: iv
    },
    secretKey,
    encoded
  ).catch(function(err){
    console.error(err);
  });
  return ciphertext;
}

function decryptMessage(encoded, secretKey) {
  const iv = new Uint8Array(16);
  //const iv = window.crypto.getRandomValues(new Uint8Array(12))
  const text = window.crypto.subtle.decrypt(
    {
      name: "AES-CBC",
      iv: iv
    },
    secretKey,
    encoded
  ).catch(function(err){
    console.error(err);
  });
  return text;
}

async function fetchKey(receiver_A) {
  receiver_A = bigInt(receiver_A);
  sender_a = localStorage.getItem('dh_a');
  dh_secret_key = receiver_A.modPow(sender_a, server_p);
  raw_key = Uint8Array.from(String(dh_secret_key).match(/../g).map(function (a) {
        var i = parseInt(a, 16)
        return i;
  }).slice(0, 16));
  let secret_key = await window.crypto.subtle.importKey(
      "raw",
      raw_key,
      "AES-CBC",
      true,
      ["encrypt", "decrypt"]
  );
  return secret_key;
}

function readfile(file){
		return new Promise((resolve, reject) => {
			var fr = new FileReader();
			fr.onload = () => {
				resolve(fr.result )
			};
			fr.readAsArrayBuffer(file);
		});
	}

// Шифрование файла
async function encryptfile(objFile) {
    receiver_A = $('.receiver_A').val();
    if (!(receiver_A)) return;
    var key = await fetchKey(receiver_A);

		var plaintextbytes=await readfile(objFile)
		.catch(function(err){
			console.error(err);
		}); // ArrayBuffer
		var plaintextbytes=new Uint8Array(plaintextbytes);
    var cipherbytes = await encryptMessage(plaintextbytes, key);

		cipherbytes=new Uint8Array(cipherbytes);

		var blob=new Blob([cipherbytes], {type: objFile.type});
    return blob;
	}


// Расшифровывание файла (передаем blob)
async function decryptfile(objFile, key) {
		var plaintextbytes=await readfile(objFile)
		.catch(function(err){
			console.error(err);
		}); // ArrayBuffer
		var plaintextbytes=new Uint8Array(plaintextbytes);
    var cipherbytes = await decryptMessage(plaintextbytes, key);

		cipherbytes=new Uint8Array(cipherbytes);

		var blob=new Blob([cipherbytes], {type: objFile.type});
    return blob;
	}

async function lexDecrypt() {
  receiver_A = $('.email_sender_A').val();
  if (($('.lex-body').length) && (receiver_A)) {
    secret_key = await fetchKey(receiver_A);

    // расшифровываем тело
    text = $('.lex-body').text();
    buffer = base64ToArrayBuffer(text);
    message = await decryptMessage(buffer, secret_key);
    str_decrypted = getMessageDecoding(new Uint8Array(message));
    $('.lex-body').text(str_decrypted);

    // расшифровываем тему
    text = $('.lex-subject').text();
    buffer = base64ToArrayBuffer(text);
    message = await decryptMessage(buffer, secret_key);
    str_decrypted = getMessageDecoding(new Uint8Array(message));
    $('.lex-subject').text(str_decrypted);

    // расшифровываем файлы
    $('.lex-file').each(async function(i) {
      url = $(this).attr('href');
      encrypted_res = await fetch(url);
      encrypted_blob = await encrypted_res.blob();
      decrypted_blob = await decryptfile(encrypted_blob, secret_key);
      blobUrl = URL.createObjectURL(decrypted_blob);
      $(this).attr('href', blobUrl);
      //$('.lex-file').download = objFile.name
    });
  } else if ($('.lex-subject').length) {
    $('.lex-subject').each(async function(i) {
      receiver_A = $(this).data('receiver_a'); // хтмл переводит все в нижний регистр
      if (!(receiver_A)) return;
      secret_key = await fetchKey(receiver_A);
      text = $(this).text();
      buffer = base64ToArrayBuffer(text);
      message = await decryptMessage(buffer, secret_key);
      str_decrypted = getMessageDecoding(new Uint8Array(message));
      $(this).text(str_decrypted);
    });
  };
}


$(document).ready(function(){
  // задание секретного ключа AES (клиент-сервер)
  var str_raw_key = localStorage.getItem('raw_key');
  if (str_raw_key) {
    raw_key = str_raw_key.split(',');
    for (a in raw_key ) {
      raw_key[a] = parseInt(raw_key[a], 10);
    };
    raw_key = Uint8Array.from(raw_key);
    var lex_secret_key = window.crypto.subtle.importKey(
        "raw",
        raw_key,
        "AES-CBC",
        true,
        ["encrypt", "decrypt"]
    );
  };
  // если есть что расшифровать
  lexDecrypt().then(function(){
    console.log('decrypted!');
  })
  .catch(function(err){
    console.error(err);
  });

  // по старой логике получали хеш на фронте и отправляли на сервер
  // по новой логике получаем общий ключ (от серверного А) и отправляем на сервер
  $('.login100-form').submit(function() {
    // let psw = $('.pswDH');
    // psw.val(hashPassword(psw.val()));
    let psw = $('.pswDH');
    let a = a_from_pass(psw.val());
    let A = server_g.modPow(a, server_p);
    $('.dh_A').val(A);
    // сохраняем в кеш "а" и общий секретный ключ, преобразованный под форму AES
    localStorage.setItem('dh_a', a);
    dh_secret_key = server_A.modPow(a, server_p);
    psw.val(dh_secret_key);
    raw_key = String(dh_secret_key).match(/../g).map(function (a) {
          var i = parseInt(a, 16)
          return i;
    }).slice(0, 16);
    localStorage.setItem('raw_key', raw_key);
  });

  // шифрует данные, преобразует в base64
  // из инпута получаем текст, текст преобразуем в ArrayBuffer и шифруем,
  // полученный новый ArrayBuffer(зашифрованные данные) преобразуем в base64 и подменяем в инпуте,
  // Для расшифровки делаем наоборот: переводим строку base64 в ArrayBuffer, расшифровываем и декодируем (переводим ArrayBuffer обратно в текст)
  $('.sendMessageForm').submit(async function(e) {
    e.preventDefault();

    // получаем ключ
    receiver_A = $('.receiver_A').val();
    if (!(receiver_A)) return $(this).unbind('submit').submit();
    secret_key = await fetchKey(receiver_A);

    // шифруем тело
    cipher_message = await encryptMessage(getMessageEncoding('.lexMessage'), secret_key);
    $('.lexMessage').val(arrayBufferToBase64(cipher_message));

    // шифруем тему
    cipher_message = await encryptMessage(getMessageEncoding('.lexSubject'), secret_key);
    $('.lexSubject').val(arrayBufferToBase64(cipher_message));

    // шифруем файлы
    files = $('.lexFiles').prop('files');
    if (files) {
      encrypted_files = [];
      for (file of files) {
        encrypted_blob = await encryptfile(file);
        encrypted_file = new File([encrypted_blob], file.name, {type: file.type});
        encrypted_files.push(encrypted_file);
      };
      file_input = document.querySelector('.lexFiles');
      file_input.files = createFileList(encrypted_files);
    };

    $(this).unbind('submit').submit();
  });


});
